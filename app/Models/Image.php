<?php

namespace App\Models;

class Image
{
    public const RESIZE_IMAGE_DIR = 'cache';
    public const SOURCE_IMAGE_DIR = 'gallery';

    public const ALLOWED_SOURCE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'webp'];
    public const RESIZE_EXTENSION = 'jpg';
    public const PREVIEW_SHOW_LIMIT = 10;
    public const DEFAULT_MOBILE_PREVIEW_SIZE = 'mic';
    public const DEFAULT_DESKTOP_PREVIEW_SIZE = 'min';
    public const EXCLUDED_MOBILE_SIZES = ['big'];
    public const EXCLUDED_DESKTOP_SIZES = ['mic'];

    public ?string $resizedPath;

    public function __construct(
        public readonly string $sourcePath,
        public readonly string $name,
        public readonly string $sizeCode,
    ) {}
}