<?php

namespace App\Helpers;

class DeviceTypeHelper
{
    public const MOBILE_AGENTS_LIST = [
        'ipad', 'iphone', 'android', 'pocket', 'palm', 'windows ce', 'windowsce', 'cellphone',
        'opera mobi', 'ipod', 'small', 'sharp', 'sonyericsson', 'symbian', 'opera mini', 'nokia',
        'htc_', 'samsung', 'motorola', 'smartphone', 'blackberry', 'playstation portable', 'tablet browser'
    ];

    public static function isMobile(): bool
    {
        $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

        foreach (self::MOBILE_AGENTS_LIST as $mobileAgent) {
            if (str_contains($userAgent, $mobileAgent)) {
                return true;
            };
        }

        return false;
    }
}