<?php

namespace App\Helpers;

use App\models\Image;

class ImageHelper
{
    public static function getGlobPatternImageExtensions(): string
    {
        return implode(',', Image::ALLOWED_SOURCE_EXTENSIONS);
    }

    public static function getSourceImageGlobPattern(string $imageName): string
    {
        return Image::SOURCE_IMAGE_DIR . '/' . $imageName . '.{' . self::getGlobPatternImageExtensions() . '}';
    }

    public static function getResizedImageGlobPattern(string $imageName, string $sizeCode): string
    {
        return Image::RESIZE_IMAGE_DIR . '/' . $imageName . '_' . $sizeCode . '.' . Image::RESIZE_EXTENSION;
    }

    public static function getImagesListGlobPattern(): string
    {
        return Image::SOURCE_IMAGE_DIR . '/*.{' . self::getGlobPatternImageExtensions() . '}';
    }
}