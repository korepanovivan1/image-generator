<?php

namespace App\config;

use PDO;

class Database
{
    public function __construct(
        private readonly string $type = 'mysql',
        private readonly string $host = 'mysql',
        private readonly string $name = 'image-generator',
        private readonly string $user = 'root',
        private readonly string $password = 'secret',
    ) {}

    public function getPdo(): PDO
    {
        return new PDO(
            $this->type . ':host=' . $this->host . ';dbname=' . $this->name,
            $this->user,
            $this->password
        );
    }
}