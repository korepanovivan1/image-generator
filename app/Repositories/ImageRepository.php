<?php

namespace App\Repositories;

use App\config\Database;
use App\Helpers\DeviceTypeHelper;
use App\Models\Image;
use PDO;
use Exception;

class ImageRepository
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = (new Database)->getPdo();
    }

    /**
     * @return array{width: int, height: int}
     * @throws Exception
     */
    public function getSizeData(string $sizeCode): array
    {
        $preparedQueryRaw = <<<SQL
            SELECT width, height 
            FROM image_sizes
            WHERE code=:code
            LIMIT 1
        SQL;

        $query = $this->pdo->prepare($preparedQueryRaw);
        $query->execute(['code' => $sizeCode]);

        if ($query->rowCount() === 0) {
            throw new Exception('Размер с кодом size не найден');
        }

        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function getAllowedImageSizes(): array
    {
        $excludedImagesSizes = implode(
            ',', DeviceTypeHelper::isMobile() ? Image::EXCLUDED_MOBILE_SIZES : Image::EXCLUDED_DESKTOP_SIZES);

        $preparedQueryRaw = <<<SQL
            SELECT code 
            FROM image_sizes
            WHERE code NOT IN(:excludedCodes)
            ORDER BY width DESC
        SQL;

        $query = $this->pdo->prepare($preparedQueryRaw);
        $query->execute(['excludedCodes' => $excludedImagesSizes]);

        return array_column($query->fetchAll(PDO::FETCH_ASSOC), 'code');
    }
}