<?php

namespace App\Services;

use App\Helpers\ImageHelper;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Exception;
use PDO;
use GdImage;

readonly class ImageConvertService
{
    private PDO $pdo;

    public function __construct(
        private Image $image,
        private readonly ImageRepository $imageRepository = new ImageRepository,
    ) {}

    /**
     * @throws Exception
     */
    public function getResizedImage(): string
    {
        ['width' => $targetWidth, 'height' => $targetHeight] = $this->imageRepository
            ->getSizeData($this->image->sizeCode);

        $sourceImage = $this->createGdImageFromMultipleExtensions($this->image->sourcePath);
        $sourceImageSize = getimagesize($this->image->sourcePath);

        if (!$sourceImage || !$sourceImageSize) {
            throw new Exception('Ошибка при получении исходного изображения');
        }

        $resizedImage = imagescale($sourceImage, $targetWidth, $targetHeight);

        if (!$resizedImage) {
            throw new Exception('Ошибка при преобразовании исходного изображения');
        }

        $this->saveConvertedImageIntoCache($resizedImage);

        return $this->image->resizedPath;
    }

    public function createGdImageFromMultipleExtensions(string $imagePath): GdImage|false
    {
        $imageExtension = pathinfo($imagePath, PATHINFO_EXTENSION);

        return match ($imageExtension) {
            'jpg', 'jpeg' => imagecreatefromjpeg($imagePath),
            'png' => imagecreatefrompng($imagePath),
            'webp' => imagecreatefromwebp($imagePath),
        };
    }

    public function saveGdImageWithMultipleExtensions(
        GdImage $image,
        string $savePath,
        string $targetExtension = Image::RESIZE_EXTENSION
    ): bool {
        return match ($targetExtension) {
            'jpg', 'jpeg' => imagejpeg($image, $savePath),
            'png' => imagepng($image, $savePath),
            'webp' => imagewebp($image, $savePath),
        };
    }

    /**
     * @throws Exception
     */
    private function saveConvertedImageIntoCache(GdImage $image): void
    {
        $resizedImagePath = ImageHelper::getResizedImageGlobPattern(
            $this->image->name,
            $this->image->sizeCode
        );

        if (!$this->saveGdImageWithMultipleExtensions($image, $resizedImagePath)) {
            throw new Exception('Не удалось сохранить преобразованное изображение');
        }

        $this->image->resizedPath = $resizedImagePath;
    }
}