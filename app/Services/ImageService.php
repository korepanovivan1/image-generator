<?php

namespace App\Services;

use App\Helpers\DeviceTypeHelper;
use App\Models\Image;
use App\Repositories\ImageRepository;
use App\Helpers\ImageHelper;
use Exception;

class ImageService
{
    public function __construct(
        private readonly ImageRepository $imageRepository = new ImageRepository,
    ) {}

    /**
     * @throws Exception
     */
    public function getResizedImagePath(string $imageName, string $sizeCode): string
    {
        if ($resizedImage = $this->getResizedImageFromCache($imageName, $sizeCode)) {
            return $resizedImage;
        }

        $imagePath = $this->getSourceImagePath($imageName);

        if (!$imagePath) {
            throw new Exception('Изображения с названием name не найдено');
        }

        $image = new Image($imagePath, $imageName, $sizeCode);

        return (new ImageConvertService($image))->getResizedImage();
    }

    public function outputImageIntoBrowser($imagePath): void
    {
        ob_clean();
        header('Content-Type: image/jpeg');

        echo file_get_contents($imagePath);
    }

    /**
     * @return array<string>
     */
    public function getPreviewImagesLinksList(int $imagesLimit = Image::PREVIEW_SHOW_LIMIT): array
    {
        $imagesListGlobPattern = ImageHelper::getImagesListGlobPattern();
        $previewImageSize = DeviceTypeHelper::isMobile(
        ) ? Image::DEFAULT_MOBILE_PREVIEW_SIZE : Image::DEFAULT_DESKTOP_PREVIEW_SIZE;

        $previewImagesLinksList = [];
        $imagesList = glob($imagesListGlobPattern, GLOB_BRACE);
        $imagesListLength = count($imagesList);

        if ($imagesListLength < $imagesLimit) {
            $imagesLimit = $imagesListLength;
        }

        for ($i = 0; $i < $imagesLimit; $i++) {
            $previewImagesLinksList[] = [
                'link' => sprintf(
                    '/generator.php?name=%s&size=%s',
                    pathinfo($imagesList[$i], PATHINFO_FILENAME),
                    $previewImageSize
                ),
                'imageName' => pathinfo($imagesList[$i], PATHINFO_FILENAME)
            ];
        }

        return $previewImagesLinksList;
    }

    public function getAllowedImageSizes(): string
    {
        return (implode(',', $this->imageRepository->getAllowedImageSizes()));
    }

    private function getSourceImagePath(string $imageName): string|false
    {
        if ($searchedImages = glob(
            ImageHelper::getSourceImageGlobPattern($imageName),
            GLOB_BRACE
        )) {
            return $searchedImages[0];
        }

        return false;
    }

    private function getResizedImageFromCache(string $imageName, string $sizeCode): string|false
    {
        if ($searchedImages = glob(ImageHelper::getResizedImageGlobPattern($imageName, $sizeCode))) {
            return $searchedImages[0];
        }

        return false;
    }
}