<?php

namespace App\Services;

use Exception;

class ValidationService
{
    /**
     * @throws Exception
     */
    public static function checkRequired(array $values): void
    {
        foreach ($values as $value) {
            if(!isset($value) || strlen($value) === 0) {
                throw new Exception('Не все обязательные параметры заполнены');
            }
        }
    }
}