Генератор изображений, более подробное описание задания в task.docx
========================================

Запуск проекта
-------------

Конфигурация окружения находится в docker-compose.
Для запуска проекта выполнить:
> cd docker
> 
> docker-compose up -d

После запуска docker установить пакеты composer и накатить дамп БД из файла image-generator_db_dump.sql.


Зайти в контейнер с php-fpm и выполнить composer install:
> composer install

Зайти в контейнер с mysql и накатить дамп БД:

> mysql -u root -p (пароль по умполчанию secret)
> 
> CREATE DATABASE IF NOT EXISTS \`image-generator\`;
> 
> use \`image-generator\`
> 
> source /var/www/image-generator_db_dump.sql

Для отображения проекта в браузере добавить в hosts файл строку
> 127.0.0.1 image-generator.test

При изменении имени домена изменить директиву **server_name** в файле **/docker/nginx/image-generator.test.conf**

Конфиг подключения к БД находится в **/app/Config/Database.php**.

Настройки проекта (названия папок для исходных и resized изображений, допустимые расширения изображений, расширение конвертируемых изображений и пр.) можно изменить в **/app/Models/Image.php**.