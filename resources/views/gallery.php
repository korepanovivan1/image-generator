<?php
/** @var array{link: string, imageName:string} $previewImagesLinksList */
/** @var string $allowedImageSizes */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ImagesGallery</title>
    <link rel="stylesheet" href="/resources/css/tiny-slider.css">
    <link rel="stylesheet" href="/resources/css/style.css">
</head>
<body>
<div class="container" data-sizes="<?= $allowedImageSizes ?>">
    <?php foreach ($previewImagesLinksList as $imageLink): ?>
        <img class="preview-image" src="<?= $imageLink['link'] ?>" data-name="<?= $imageLink['imageName'] ?>"
             alt="gallery-image-preview">
    <?php endforeach; ?>
</div>
<div class="modal-container">
    <div class="modal-slider">
        <div class="slider">
            <?php foreach ($previewImagesLinksList as $imageLink): ?>
                <div class="slider-item">
                    <img class="full-image" src="<?= $imageLink ?>">
                </div>
            <?php endforeach; ?>
        </div>
        <div class="modal-close">X</div>
    </div>
</div>
<script src="/resources/js/jquery.min.js"></script>
<script src="/resources/js/tiny-slider.min.js"></script>
<script src="/resources/js/script.js"></script>
</body>
</html>
