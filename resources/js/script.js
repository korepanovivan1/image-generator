let slider = null;

$('.preview-image').click(function () {
    $('.modal-container').addClass('active');

    let sliderImageExtensions = $('.container').data('sizes').split(',');
    let imageName = $(this).data('name');

    let imageSliderAppendHtml = '';
    sliderImageExtensions.forEach((extension) => {
        imageSliderAppendHtml += `<img src="/generator.php?name=${imageName}&size=${extension}" alt="gallery-image">`;
    })

    $('.slider').html(() => {
        return imageSliderAppendHtml;
    })

    slider = tns({
        container: '.slider',
        items: 1,
        autoplay: true,
        autoWidth: true,
        center: true
    });
});

$('.modal-close').click(() => {
    $('.modal-container').removeClass('active');
    slider.destroy();
});