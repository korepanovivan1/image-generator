<?php

use App\Services\ImageService;
use App\Services\ValidationService;

require_once 'vendor/autoload.php';

$name = $_GET['name'] ?? null;
$sizeCode = $_GET['size'] ?? null;

try {
    ValidationService::checkRequired([$name, $sizeCode]);

    $imageService = new ImageService;
    $imagePath = $imageService->getResizedImagePath($name, $sizeCode);

    $imageService->outputImageIntoBrowser($imagePath);
} catch (Throwable $e) {
    echo 'Ошибка: ' . $e->getMessage();
}

